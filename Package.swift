// swift-tools-version:5.7
import PackageDescription

let package = Package(
    name: "YandexPaySDK",
    products: [
        .library(
            name: "YandexPaySDK",
            targets: ["YandexPaySDK"]
        ),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(name: "YandexPaySDK",
                      url: "https://yandexpay-ios-sdk.s3.yandex.net/1.2.1/YandexPaySDK_121222_7524285.xcframework.zip",
                      checksum: "714698b6e5ff407a76304ce8cf7801fcfc143e432157a1c6624abb04c51bfc2e")
    ]
)
